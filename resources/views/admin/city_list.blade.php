@extends('layouts.main')

@section('title')
   Upcoming Rides  | {{env('SITE_NAME')}}
@endsection

@section('content')
    <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">

            <div class="box box-primary">
                <div class="box-header with-border">
                    <div class="col-md-10">
                        <h3 class="box-title " style="margin-top: 5px;"> List Of City</h3>
                    </div>
                    <div class="col-md-2 ">

                    <a href="#" class="btn btn-block btn-primary btn-sm" data-toggle="modal" data-target="#AddCityModel">Add New City</a>

                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>State</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($cities as $city)
                            <tr>
                                <td>{{$city->id}}</td>
                                <td>{{$city->name}}</td>
                                <td>{{$city->state}}</td>
                                <td>

                                    <a  data-val="{{ $city->id }}"  data-val-status="{{ $city->status }}"
                                        id="status_{{ $city->id }}"
                                        class="status_check @if($city->status == \App\City::Active) {{'label label-success'}} @else {{'label label-danger'}} @endif">@if($city->status == \App\City::Active)  {{ 'Active' }} @else {{'De-Active'}} @endif
                                    </a>
                                </td>
                                <td class="text-center" width="140px">
                                    {{--<a href="javascript:void(0)" class="editicon editcity"> <i class="fa fa-pencil-square fa-2x margin-r-15 margin-l-15" aria-hidden="true"></i></a>--}}
                                    <a href="javascript:void(0)" class="deleteicon deletecity"  data-val="{{$city->id}}"> <i class="fa fa-trash fa-2x " aria-hidden="true"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>State</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->



    <!---- Add New City Model -->
    <div class="modal fade" id="AddCityModel" data-keyboard="false" data-backdrop="false" aria-hidden="true">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Add New City</h4>
                    </div>
                    <form id="addNewCityFrm" method="post" action="{{route('admin.addCity')}}">
                        {{csrf_field()}}
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">City Name</label>
                                <input type="text" class="form-control" id="searchCity" name="searchCity" placeholder="Enter City Name">
                                <input type="hidden" name="latitude" id="latitude">
                                <input type="hidden" name="longitude" id="longitude">
                                <input type="hidden" name="state" id="state">
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!----End  Add New City Model -->
    <!---- Edit City Model -->
    <div class="modal fade" id="EditCityModel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit City</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">City Name</label>
                        <input type="text" class="form-control" id="searchCity1" placeholder="Enter City Name">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary">Update</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!----End  Edit City Model -->
@endsection


@section('js')


    <script>
        $(function () {
            $('#example1').DataTable();
        })

        $('.editcity').click(function () {
            $('#EditCityModel').modal('show');
        })

        function initialize() {

            var input = document.getElementById('searchCity');
            var options = {
                types: ['(cities)'],
                componentRestrictions: {country: 'IN'}
            };

            var autocomplete = new google.maps.places.Autocomplete($("#searchCity")[0], options);

            google.maps.event.addListener(autocomplete, 'place_changed', function() {
                var place = autocomplete.getPlace();

                var addcomponent=place.address_components;
                var adraddress=place.adr_address;
                console.log(addcomponent);

                var region='';
                var city='';

                for (var i=0; i<addcomponent.length; i++) {
                    for (var b=0;b<addcomponent[i].types.length;b++) {

                        if (addcomponent[i].types[b] == "administrative_area_level_1") {
                            region = addcomponent[i].long_name;
                            break;
                        }
                        if (addcomponent[i].types[b] == "locality") {
                            city = addcomponent[i].long_name;
                            break;
                        }
                    }
                }

                $('#searchCity').val(city);
                $('#state').val(region);
                $('#longitude').val(place.geometry.location.lng());
                $('#latitude').val(place.geometry.location.lat());

            });

        }

        $('.status_check').click(function () {
            var status = $(this).attr('data-val-status');
            var cityId = $(this).attr('data-val');

            if(status==1)
            {
                var msg='De-Activate'
            }
            else
            {
                var msg='Activate'
            }
            swal({
                title: 'Are you sure?',
                text: "You want to " + msg + " this City!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function () {
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.changeStatusCity') }}",
                    data: {
                        _token: '{{ csrf_token() }}',
                        status: status,
                        cityId: cityId
                    },
                    success: function (data) {
                        $('.uk-modal-dialog-replace').hide();
                        var element = $('#status_' + cityId);
                        if (data['status']==1) {
                            var msg='Activated';
                            element.html('Active')
                                .removeClass('label label-danger')
                                .addClass('label label-success');
                        } else {
                            var msg='De-Activated';
                            element.html('De-Active')
                                .removeClass('label label-success')
                                .addClass('label label-danger');
                        }
                        element.val(data['status']);

                        swal({
                            title: 'Good job',
                            text: 'City has been ' + msg + '.',
                            type: 'success',
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'Yes'
                        }).then(function () {
                            window.location.reload();
                        });
                    },
                    error: function (xhr, status, error) {
                        $('.overlay').css('display', 'none');
                        swal(
                            'Oops !',
                            'Something went wrong, please try again later.',
                            'warning'
                        );
                    }
                });
            });
        });

        $('.deletecity').on('click', function(e){
            var deleteCity = $(this).attr('data-val');
            swal({
                title: 'Are you sure?',
                text: "You want to Delete this City",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then(function () {
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.deleteCity') }}",
                    data: {
                        _token: '{{ csrf_token() }}',
                        deleteCity: deleteCity
                    },
                    success: function (data) {
                        swal({
                            title: 'Success!',
                            text: 'City has been removed Successfully.',
                            type: "success",
                            confirmButtonText: "OK"
                        }).then(function () {
                            location.reload();
                        });
                    },
                    error: function (xhr, status, error) {
                        swal(
                            'Oops !',
                            'Something went wrong, please try again later.',
                            'warning'
                        );
                    }
                });
            });
        });

    </script>
@endsection
