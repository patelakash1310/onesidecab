@extends('layouts.main')

@section('title')
   Upcoming Rides  | {{env('SITE_NAME')}}
@endsection

@section('content')
    <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">

                    <!-- About Me Box -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <div class="col-md-10">
                                <h3 class="box-title " style="margin-top: 5px;"> List Of Order</h3>
                            </div>
                            {{--<div class="col-md-2 ">--}}

                                {{--<a href="{{route('addMember')}}" class="btn btn-block btn-primary btn-sm">Add New Member</a>--}}

                            {{--</div>--}}
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>

            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile No</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
    <td>1</td>
    <td>ABC123</td>
    <td>John</td>
    <td>John@gmail.com</td>
    <td>9898989898</td>
    <td class="text-center" width="140px">
        <a href="#" class="viewicon"> <i class="fa fa-eye fa-2x" aria-hidden="true"></i></a>
        <a href="#" class="editicon"> <i class="fa fa-pencil-square fa-2x margin-r-15 margin-l-15" aria-hidden="true"></i></a>
        <a href="#" class="deleteicon deletemember"  data-val=""> <i class="fa fa-trash fa-2x " aria-hidden="true"></i></a>
    </td>
</tr>
                            <tr>
                                <td>1</td>
                                <td>ABC123</td>
                                <td>John</td>
                                <td>John@gmail.com</td>
                                <td>9898989898</td>
                                <td class="text-center" width="140px">
                                    <a href="#" class="viewicon"> <i class="fa fa-eye fa-2x" aria-hidden="true"></i></a>
                                    <a href="#" class="editicon"> <i class="fa fa-pencil-square fa-2x margin-r-15 margin-l-15" aria-hidden="true"></i></a>
                                    <a href="#" class="deleteicon deletemember"  data-val=""> <i class="fa fa-trash fa-2x " aria-hidden="true"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>ABC123</td>
                                <td>John</td>
                                <td>John@gmail.com</td>
                                <td>9898989898</td>
                                <td class="text-center" width="140px">
                                    <a href="#" class="viewicon"> <i class="fa fa-eye fa-2x" aria-hidden="true"></i></a>
                                    <a href="#" class="editicon"> <i class="fa fa-pencil-square fa-2x margin-r-15 margin-l-15" aria-hidden="true"></i></a>
                                    <a href="#" class="deleteicon deletemember"  data-val=""> <i class="fa fa-trash fa-2x " aria-hidden="true"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>ABC123</td>
                                <td>John</td>
                                <td>John@gmail.com</td>
                                <td>9898989898</td>
                                <td class="text-center" width="140px">
                                    <a href="#" class="viewicon"> <i class="fa fa-eye fa-2x" aria-hidden="true"></i></a>
                                    <a href="#" class="editicon"> <i class="fa fa-pencil-square fa-2x margin-r-15 margin-l-15" aria-hidden="true"></i></a>
                                    <a href="#" class="deleteicon deletemember"  data-val=""> <i class="fa fa-trash fa-2x " aria-hidden="true"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>ABC123</td>
                                <td>John</td>
                                <td>John@gmail.com</td>
                                <td>9898989898</td>
                                <td class="text-center" width="140px">
                                    <a href="#" class="viewicon"> <i class="fa fa-eye fa-2x" aria-hidden="true"></i></a>
                                    <a href="#" class="editicon"> <i class="fa fa-pencil-square fa-2x margin-r-15 margin-l-15" aria-hidden="true"></i></a>
                                    <a href="#" class="deleteicon deletemember"  data-val=""> <i class="fa fa-trash fa-2x " aria-hidden="true"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>ABC123</td>
                                <td>John</td>
                                <td>John@gmail.com</td>
                                <td>9898989898</td>
                                <td class="text-center" width="140px">
                                    <a href="#" class="viewicon"> <i class="fa fa-eye fa-2x" aria-hidden="true"></i></a>
                                    <a href="#" class="editicon"> <i class="fa fa-pencil-square fa-2x margin-r-15 margin-l-15" aria-hidden="true"></i></a>
                                    <a href="#" class="deleteicon deletemember"  data-val=""> <i class="fa fa-trash fa-2x " aria-hidden="true"></i></a>
                                </td>
                            </tr>
                        {{--@foreach($members as $member)--}}
                            {{--<tr>--}}
                                {{--<td>{{$member->id}}</td>--}}
                                {{--<td>{{$member->code}}</td>--}}
                                {{--<td>{{$member->name}}</td>--}}
                                {{--<td>{{$member->email}}</td>--}}
                                {{--<td> {{$member->mobile_no}}</td>--}}
                                {{--<td class="text-center" width="140px">--}}
                                    {{--@permission('read.member')--}}
                                    {{--<a href="{{route('viewMember',['id' => $member->id])}}" class="viewicon"> <i class="fa fa-eye fa-2x" aria-hidden="true"></i></a>--}}
                                    {{--@endpermission--}}
                                    {{--@permission('update.member')--}}
                                    {{--<a href="{{route('editMember',['id' => $member->id])}}" class="editicon"> <i class="fa fa-pencil-square fa-2x margin-r-15 margin-l-15" aria-hidden="true"></i></a>--}}
                                    {{--@endpermission--}}
                                    {{--@permission('delete.member')--}}
                                    {{--<a href="#" class="deleteicon deletemember"  data-val="{{ $member->id }}"> <i class="fa fa-trash fa-2x " aria-hidden="true"></i></a>--}}
                                    {{--@endpermission--}}
                                {{--</td>--}}
                            {{--</tr>--}}

                        {{--@endforeach--}}
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Id</th>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile No</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

@endsection


@section('js')
    <script>
        $(function () {
            $('#example1').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
            })
//            $('#example2').DataTable({
//                'paging'      : true,
//                'lengthChange': false,
//                'searching'   : true,
//                'ordering'    : true,
//                'info'        : true,
//                'autoWidth'   : false
//            })
        })


        {{--$('.deletemember').on('click', function(e){--}}
            {{--var userId = $(this).attr('data-val');--}}
            {{--swal({--}}
                {{--title: 'Are you sure?',--}}
                {{--text: "You want to Delete this Member",--}}
                {{--type: 'warning',--}}
                {{--showCancelButton: true,--}}
                {{--confirmButtonColor: '#3085d6',--}}
                {{--cancelButtonColor: '#d33',--}}
                {{--confirmButtonText: 'Yes'--}}

            {{--}).then(function () {--}}

                {{--$("#loaderModel").modal('show');--}}
                {{--$.ajax({--}}
                    {{--type: "POST",--}}
                    {{--url: "{{ route('deleteMember') }}",--}}
                    {{--data: {--}}
                        {{--_token: '{{ csrf_token() }}',--}}
                        {{--userId: userId--}}
                    {{--},--}}
                    {{--success: function (data) {--}}
                        {{--$("#loaderModel").modal('hide');--}}
                        {{--setTimeout(function () {--}}

                            {{--swal({--}}
                                {{--title: 'Success!',--}}
                                {{--text: 'Member has been removed Successfully.',--}}
                                {{--type: "success",--}}
                                {{--confirmButtonText: "OK"--}}
                            {{--}).then(function () {--}}

                                {{--location.reload();--}}

                            {{--});--}}

                        {{--}, 1000);--}}

                    {{--},--}}
                    {{--error: function (xhr, status, error) {--}}
                        {{--$('.uk-modal-dialog-replace').hide();--}}
                        {{--swal(--}}
                            {{--'Oops !',--}}
                            {{--'Something went wrong, please try again later.',--}}
                            {{--'warning'--}}
                        {{--);--}}
                    {{--}--}}
                {{--});--}}

            {{--});--}}
        {{--});--}}

    </script>
@endsection
