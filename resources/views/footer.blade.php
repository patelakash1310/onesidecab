

    <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
            Anything you want
        </div>
        <!-- Default to the left -->
        <strong>Copyright © 2015 <a href="#">Company</a>.</strong> All rights reserved.
    </footer>

    <div class="modal fade" id="loaderModel" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Please Wait ....</h4>
                </div>
                <div class="modal-body">
                    <div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>
                </div>

            </div>

        </div>
    </div>
</div><!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 3 -->
    <script src="{{asset('bower_components/jquery/dist/jquery.min.js')}}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{asset('bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <!-- Bootstrap 3.3.7 -->
    <script src="{{asset('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- Select2 -->
    <script src="{{asset('bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <!-- DataTables -->
    <script src="{{asset('/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <!-- daterangepicker -->
    <script src="{{asset('bower_components/moment/min/moment.min.js')}}"></script>
    <script src="{{asset('bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <!-- datepicker -->
    <script src="{{asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="{{asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>

    <!-- Slimscroll -->
    <script src="{{asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    {{--<script src="{{asset('bower_components/bootstrap-multiselect/js/bootstrap-select.min.js')}}"></script>--}}
    <!-- FastClick -->
    <script src="{{asset('bower_components/fastclick/lib/fastclick.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('dist/js/adminlte.min.js')}}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAViTffNOvNyUnIbD-PVf8V0G5bIg51EnA&libraries=places&callback=initialize" async defer></script>

    <!-- AdminLTE for demo purposes -->
    <script src="{{asset('dist/js/demo.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('sweetalert2/sweetalert2.css') }}">
    <script src="{{asset('sweetalert2/sweetalert2.js') }}"></script>
    @if(Session::has('success_message'))

        <script>
            swal({
                title: 'Success!',
                text: '{{ Session::get('success_message') }}',
                type: 'success',
                confirmButtonText: 'Ok'
            })
        </script>

    @endif
    @if(Session::has('error_message'))
        <script>
            swal({
                title: 'Error!',
                text: '{{ Session::get('error_message') }}',
                type: 'error',
                confirmButtonText: 'Cancel'
            })

        </script>

    @endif

<script>

    function showLoader()
    {
        $("#loaderModel").modal('show');
    }

    function hideLoader()
    {
        $("#loaderModel").modal('hide');
    }
</script>

    @yield('js')
<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience -->
</body>
</html>