<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="{{ Request::is( 'admin/dashboard')  ? 'active' : '' }}">
                <a href="{{route('admin.Dashboard')}}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <li class="treeview @if(Request::is( 'admin/upcomming/ride')) menu-open @endif">
                <a href="#">
                    <i class="fa fa-pie-chart"></i>
                    <span>Rides</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu" @if(Request::is( 'admin/upcomming/ride')) style="display: block;" @endif>
                    <li @if(Request::is( 'admin/upcomming/ride')) class="active" @endif><a href="{{route('admin.UpcomingRides')}}"><i class="fa fa-circle-o"></i> Upcoming Rides</a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Assigned Rides</a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Completed Rides</a></li>
                </ul>
            </li>
            <li class="">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Drivers</span>
                </a>
            </li>
            <li class="">
                <a href="{{route('admin.cityMgnt')}}">
                    <i class="fa fa-dashboard"></i> <span>City Management</span>
                </a>
            </li>



            <li class="treeview">
                <a href="#">
                    <i class="fa fa-laptop"></i>
                    <span>Reports</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="pages/UI/general.html"><i class="fa fa-circle-o"></i> General</a></li>
                    <li><a href="pages/UI/icons.html"><i class="fa fa-circle-o"></i> Icons</a></li>
                    <li><a href="pages/UI/buttons.html"><i class="fa fa-circle-o"></i> Buttons</a></li>
                    <li><a href="pages/UI/sliders.html"><i class="fa fa-circle-o"></i> Sliders</a></li>
                    <li><a href="pages/UI/timeline.html"><i class="fa fa-circle-o"></i> Timeline</a></li>
                    <li><a href="pages/UI/modals.html"><i class="fa fa-circle-o"></i> Modals</a></li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
