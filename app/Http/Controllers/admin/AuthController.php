<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function index()
    {
        return view('admin.login');
    }

    public function Logout()
    {
        return view('admin.login');
    }

    public function Dashboard()
    {
        return view('admin.dashboard');
    }
}
