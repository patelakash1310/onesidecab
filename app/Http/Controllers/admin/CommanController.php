<?php

namespace App\Http\Controllers\Admin;
use App\City;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CommanController extends Controller
{
    public function cityManagement()
    {
        $cities=City::all();
        return view('admin.city_list',['cities'=>$cities]);
    }

    public function addCity(Request $request)
    {
        City::create([
           'name'=>$request->searchCity,
           'state'=>$request->state,
           'latitude'=>$request->latitude,
           'longitude'=>$request->longitude,
        ]);

        return redirect(route('admin.cityMgnt'))->with('success_message','City Added Successfully');
    }

    public function changeStatusCity(Request $request)
    {
        $this->validate($request, [
            'cityId' => 'required|exists:cities,id'
        ]);

        $city =  City::find($request['cityId']);

        if ($request['status']==1) {
            $status=$city->status = '2';
        } else {
            $status= $city->status = '1';
        }

        $city->save();

        return response()->json([
            'status' => $status
        ]);
    }

    public function deleteCity(Request $request)
    {
        $this->validate($request, [
            'deleteCity' => 'required|exists:cities,id'
        ]);
        $city =City::find($request->deleteCity);
        $city->delete();
    }
}
