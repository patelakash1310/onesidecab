<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RideController extends Controller
{
    public function UpcomingRides()
    {
        return view('admin.upcoming_rides');
    }
}
