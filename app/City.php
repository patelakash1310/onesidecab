<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable=['name','state','latitude','longitude','status'];
    const Active=1,Deactive=2;
}
