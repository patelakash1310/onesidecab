<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group([
    'namespace' => 'Admin', // it is added in controller function name
    'prefix' => 'admin', //it is added in url
    'as' => 'admin.' //route name
], function () {
    Route::get('', 'AuthController@index');
    Route::get('logout', 'AuthController@Logout')->name('Logout');
    Route::get('dashboard', 'AuthController@Dashboard')->name('Dashboard');

    Route::get('upcomming/ride', 'RideController@UpcomingRides')->name('UpcomingRides');
    Route::get('city/list', 'CommanController@cityManagement')->name('cityMgnt');
    Route::post('city/add', 'CommanController@addCity')->name('addCity');
    Route::post('city/status/change', 'CommanController@changeStatusCity')->name('changeStatusCity');
    Route::post('city/delete', 'CommanController@deleteCity')->name('deleteCity');
});
